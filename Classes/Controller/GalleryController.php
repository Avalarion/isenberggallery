<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bastian Bringenberg <typo3@bastian-bringenberg.de>, Bastian Bringenberg
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package isenberggallery
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_Isenberggallery_Controller_GalleryController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * galleryRepository
	 *
	 * @var Tx_Isenberggallery_Domain_Repository_GalleryRepository
	 */
	protected $galleryRepository;

	/**
	 * injectGalleryRepository
	 *
	 * @param Tx_Isenberggallery_Domain_Repository_GalleryRepository $galleryRepository
	 * @return void
	 */
	public function injectGalleryRepository(Tx_Isenberggallery_Domain_Repository_GalleryRepository $galleryRepository) {
		$this->galleryRepository = $galleryRepository;
	}

	/**
	 * action show
	 *
	 * @return void
	 */
	public function showAction() {
		$galleries;
		$this->view->assign('prefix', $this->settings['flexform']['prefix']);
		if($this->settings['flexform']['gallery'] === ''){
			$galleries = $this->galleryRepository->findAll();
		}else{
			$uids = explode(',', $this->settings['flexform']['gallery']);
			foreach($uids as $uid){
				$galleries[] = $this->galleryRepository->findOneByUid($uid);
			}
		}
		$pictures = array();
		foreach($galleries as $g){
			$tmp = explode(',', $g->getImages());
			$pictures = array_merge($pictures, $tmp);
		}
		if($this->settings['flexform']['gallery'] === ''){
			shuffle($pictures);
			$pictures = array_slice($pictures, 0, 40);
		}
		$this->view->assign('pictures', $pictures);
	}

}
?>
