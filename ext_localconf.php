<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Main',
	array(
		'Gallery' => 'show',
		
	),
	// non-cacheable actions
	array(
		'Gallery' => '',
		
	)
);

?>