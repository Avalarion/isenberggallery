<?php

########################################################################
# Extension Manager/Repository config file for ext "isenberggallery".
#
# Auto generated 14-10-2012 18:10
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Isenberg Gallery',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Bastian Bringenberg',
	'author_email' => 'typo3@bastian-bringenberg.de',
	'author_company' => 'Bastian Bringenberg',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.5-0.0.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:23:{s:12:"ext_icon.gif";s:4:"e922";s:17:"ext_localconf.php";s:4:"137b";s:14:"ext_tables.php";s:4:"e36d";s:14:"ext_tables.sql";s:4:"e7ab";s:21:"ExtensionBuilder.json";s:4:"b653";s:40:"Classes/Controller/GalleryController.php";s:4:"c62d";s:32:"Classes/Domain/Model/Gallery.php";s:4:"5c06";s:47:"Classes/Domain/Repository/GalleryRepository.php";s:4:"e6b1";s:44:"Configuration/ExtensionBuilder/settings.yaml";s:4:"3096";s:29:"Configuration/TCA/Gallery.php";s:4:"c658";s:38:"Configuration/TypoScript/constants.txt";s:4:"dea0";s:34:"Configuration/TypoScript/setup.txt";s:4:"36e2";s:40:"Resources/Private/Language/locallang.xml";s:4:"4d93";s:84:"Resources/Private/Language/locallang_csh_tx_isenberggallery_domain_model_gallery.xml";s:4:"ed63";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"16ae";s:38:"Resources/Private/Layouts/Default.html";s:4:"dc16";s:50:"Resources/Private/Partials/Gallery/Properties.html";s:4:"7412";s:45:"Resources/Private/Templates/Gallery/Show.html";s:4:"11d7";s:35:"Resources/Public/Icons/relation.gif";s:4:"e615";s:66:"Resources/Public/Icons/tx_isenberggallery_domain_model_gallery.gif";s:4:"905a";s:47:"Tests/Unit/Controller/GalleryControllerTest.php";s:4:"7173";s:39:"Tests/Unit/Domain/Model/GalleryTest.php";s:4:"4b90";s:14:"doc/manual.sxw";s:4:"8d2d";}',
);

?>